// console.log("wew");

// Global Objects
// Arrays

let students = [

  "Tony",
  "Peter",
  "Wanda",
  "Vision",
  "Loki"
];
// console.log(students);

// What array method can we use to add an item at the end of the array?
students.push("Thor");
// console.log(students);

// What array method can we use to add an item at the start of the array?
students.unshift("Steve");
// console.log(students);

// What array method does the opposite of push()?
students.pop();
// console.log(students);

// What array methods does the opposite of unshift()?
students.shift();
// console.log(students);

// What is the difference between splice() and slice()?
    // .splice() - removes and add items from a starting index
    // .slice() - copies a portion from starting index and returns new array from it (Non-Mutator methods)

// Another kind of iterator methods?
    // Iterator methods - loops over the items of an array
// forEach() - loops over items in an array and repeats a user-defined function
// map() - loops over items in an array and repeats a user-defined function AND returns a new array
// every() - loops and checks if all items satisfy a given condition, returns a boolean value

let arrNum = [15, 20, 25, 30, 11];

// check if every item in arrNum is divisible by 5
let allDivisible;

arrNum.forEach(num => {

  if (num % 5 === 0){
    console.log(`${num} is divisible by 5`)
  } else {
    allDivisible = false;
  }
  // However, can forEach() return data that will tell us numbers/ items in our arrNum array is divisible by 5?
})
// console.log(allDivisible);
arrNum.pop();
arrNum.push(35);
// console.log(arrNum);

let divisibleby5 = arrNum.every(num => {

  console.log(num);
  return num % 5 === 0;
})
// console.log(divisibleby5);
// result: true

// Math
// mathematical constants
// 8 pre-defined properties which can be called via the syntax Math.property (note that properties are case-sensitive)
console.log(Math);
// console.log(Math.E); // Euler's number
// console.log(Math.PI); // Pi
// console.log(Math.SQRT2); // squareroot of 2
// console.log(Math.SQRT1_2); // squareroot of 1/2
// console.log(Math.LN2); // natural logarithm of 2
// console.log(Math.LN10); // base 2 logarithm of 10
// console.log(Math.LOG2E); // base 2 logarithm of E
// console.log(Math.LOG10E); // base 10 logarithm of E

// methods for rounding a number to an integer
// console.log(Math.round(Math.PI)); // rounds to a nearest integer; result: 3
// console.log(Math.ceil(Math.PI)); // rounds UP to nearest integer; result: 4
// console.log(Math.floor(Math.PI)); // rounds DOWN to nearest integer; result: 3
// console.log(Math.trunc(Math.PI)); // returns only the integer part(ES6 update); result: 3

// // metho for returning the square root of a number
// console.log(Math.sqrt(3.14)); // result: 1.77
// // lowest value in a list of arguments
// console.log(Math.min(-4, -3, -2, -1, 0, 1 ,2 ,3 ,4)); // result: -4
// // highest value in a list of arguments
// console.log(Math.max(-4, -3, -2, -1, 0, 1 ,2 ,3 ,4)); // result: 4

// Activity
console.log(students)

// 1
function addToEnd (arrname,studentName) {

  if(typeof studentName !== "string"){
    return "error - can only add strings to an array";
  } else {
     arrname.push(studentName);
     return arrname;
  }
}

// 2
function addToStart (arrname,studentName) {

  if(typeof studentName !== "string"){
    return "error - can only add strings to an array";
  } else {
     arrname.unshift(studentName);
     return arrname;
  }
}

// 3
function elementChecker (arrname,studentName) {

  if(arrname.length !== 0){
    return true;
  } else {
    return "error - passed in array is empty"
  }
}

// 4
function checkAllStringsEnding (arrname,studentName) {

  if(arrname.length === 0){
    return "error - array must NOT be empty";
  }

  arrname.forEach(data => {
    
    if(typeof data !== 'string'){
      return "error - all array elements must be strings";
    } else {
      return;
    }
  })

  if(studentName.length > 1){
    return "error - 2nd argument must be a single character";
  }

  if(typeof studentName !== 'string'){
    return "error - 2nd argument must be of data type string";
  }
  
}

// 5
function stringLengthSorter (arrname) {

  const check = arrname.forEach(x => {

    if(typeof x !== 'string'){
      return true;
    } else {
      return false;
    }
  })

  console.log(check)

  if(check === true){
    return "error - all array elements must be strings";
  }

  if(arrname.length !== 0){
    arrname.sort()
    return arrname;
  }

}
